#!/usr/bin/env bash

helm upgrade $1-specific ./environments/$1 --debug --kube-context=$1.speedspa.tep.pw
helm upgrade $1-general ./ -f ./environments/$1/values.yaml --set env=$1 --debug --kube-context=$1.speedspa.tep.pw
