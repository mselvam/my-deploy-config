#!/usr/bin/env bash

helm init
helm install --name=$1-specific ./environments/$1 --set env=$1 --kube-context=useast1.dev-speedspa.novelonline.info
helm install --name=$1-general ./ -f ./environments/$1/values.yaml --set env=$1 --kube-context=useast1.dev-speedspa.novelonline.info
